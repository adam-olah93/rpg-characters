package Test;

import static org.junit.Assert.*;

import Main.*;
import Main.characters.*;
import Main.exceptions.*;
import Main.items.*;
import org.junit.*;

public class ItemsAndEquipmentTest {

    @Test
    public void mageEquipWeapon_levelTwoHeight_throwsException() {
        Mage mage = new Mage("Mage");
        Weapon mageWeapon1 = new Weapon("mageWeapon1", 2, Slot.Weapon, 5, 2, WeaponType.Staff );
        assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(mageWeapon1);
        });
    }

    @Test
    public void rangerEquipWeapon_levelTwoHeight_throwsException() {
        Ranger ranger = new Ranger("Ranger");
        Weapon rangerWeapon1 = new Weapon("rangerWeapon1", 2, Slot.Weapon, 6, 4,WeaponType.Bow );
        assertThrows(InvalidWeaponException.class, () -> {
            ranger.equipWeapon(rangerWeapon1);
        });
    }

    @Test
    public void rogueEquipWeapon_levelTwoHeight_throwsException() {
        Rogue rogue = new Rogue("Rogue");
        Weapon rogueWeapon1 = new Weapon("rogueWeapon1",2, Slot.Weapon, 6, 8,WeaponType.Dagger);
        assertThrows(InvalidWeaponException.class, () -> {
            rogue.equipWeapon(rogueWeapon1);
        });
    }

    @Test
    public void warriorEquipWeapon_levelTwoHeight_throwsException() {
        Warrior warrior = new Warrior("Main.Warrior");
        Weapon warriorWeapon1 = new Weapon("warriorWeapon1", 2, Slot.Weapon, 10, 1,WeaponType.Axe);
        Assert.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(warriorWeapon1);
        });
    }

    @Test
    public void mageEquipWeapon_wrongType_throwsException() {
        Mage mage = new Mage("Mage");
        Weapon mageWeapon1 = new Weapon("mageWeapon1", 1, Slot.Weapon, 5, 2, WeaponType.Dagger );
        Assert.assertThrows(InvalidWeaponException.class, () -> {
            mage.equipWeapon(mageWeapon1);
        });
    }

    @Test
    public void rangerEquipWeapon_wrongType_throwsException() {
        Ranger ranger = new Ranger("Ranger");
        Weapon rangerWeapon1 = new Weapon("rangerWeapon1", 1, Slot.Weapon, 6, 4,WeaponType.Dagger );
        Assert.assertThrows(InvalidWeaponException.class, () -> {
            ranger.equipWeapon(rangerWeapon1);
        });
    }

    @Test
    public void rogueEquipWeapon_wrongType_throwsException() {
        Rogue rogue = new Rogue("Rogue");
        Weapon rogueWeapon1 = new Weapon("rogueWeapon1",1, Slot.Weapon, 6, 8,WeaponType.Staff);
        Assert.assertThrows(InvalidWeaponException.class, () -> {
            rogue.equipWeapon(rogueWeapon1);
        });
    }

    @Test
    public void warriorEquipWeapon_wrongType_throwsException() {
        Warrior warrior = new Warrior("Main.Warrior");
        Weapon warriorWeapon1 = new Weapon("warriorWeapon1", 2, Slot.Weapon, 10, 1,WeaponType.Staff);
        Assert.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(warriorWeapon1);
        });
    }


    @Test
    public void mageEquipArmor_levelTwoHeight_throwsException() {
        Mage mage = new Mage("Mage");
        Armor mageArmor = new Armor("mageArmor",2,Slot.Head,ArmorType.Cloth,1,2,3);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(mageArmor);
        });
    }

    @Test
    public void rangerEquipArmor_levelTwoHeight_throwsException() {
        Ranger ranger = new Ranger("Ranger");
        Armor rangerArmor = new Armor("rangerArmor",2, Slot.Body, ArmorType.Leather, 7,5,1);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            ranger.equipArmor(rangerArmor);
        });
    }

    @Test
    public void rogueEquipArmor_levelTwoHeight_throwsException() {
        Rogue rogue = new Rogue("Rogue");
        Armor rogueArmor = new Armor("rogueArmor",2, Slot.Body, ArmorType.Leather, 5,6,2);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            rogue.equipArmor(rogueArmor);
        });
    }

    @Test
    public void warriorEquipArmor_levelTwoHeight_throwsException() {
        Warrior warrior = new Warrior("Main.Warrior");
        Armor warriorArmor = new Armor("warriorArmor",2, Slot.Body, ArmorType.Plate, 2,7,2);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(warriorArmor);
        });
    }

    @Test
    public void mageEquipArmor_wrongType_throwsException() {
        Mage mage = new Mage("Mage");
        Armor mageArmor = new Armor("mageArmor",1,Slot.Head,ArmorType.Plate,1,2,3);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            mage.equipArmor(mageArmor);
        });
    }

    @Test
    public void rangerEquipArmor_wrongType_throwsException() {
        Ranger ranger = new Ranger("Ranger");
        Armor rangerArmor = new Armor("rangerArmor",1, Slot.Body, ArmorType.Plate, 7,5,1);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            ranger.equipArmor(rangerArmor);
        });
    }

    @Test
    public void rogueEquipArmor_wrongType_throwsException() {
        Rogue rogue = new Rogue("Rogue");
        Armor rogueArmor = new Armor("rogueArmor",1, Slot.Body, ArmorType.Plate, 5,6,2);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            rogue.equipArmor(rogueArmor);
        });
    }

    @Test
    public void warriorEquipArmor_wrongType_throwsException() {
        Warrior warrior = new Warrior("Main.Warrior");
        Armor warriorArmor = new Armor("warriorArmor",1, Slot.Body, ArmorType.Cloth, 2,7,2);
        Assert.assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(warriorArmor);
        });
    }

    @Test
    public void mageEquipWeapon_correct_returnTrue() throws InvalidWeaponException{
        Mage mage = new Mage("Mage");
        Weapon mageWeapon1 = new Weapon("mageWeapon1", 1, Slot.Weapon, 5, 2, WeaponType.Staff );
        boolean b =  mage.equipWeapon(mageWeapon1);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void rangerEquipWeapon_correct_returnTrue() throws InvalidWeaponException{
        Ranger ranger = new Ranger("Ranger");
        Weapon rangerWeapon1 = new Weapon("rangerWeapon1", 1, Slot.Weapon, 6, 4,WeaponType.Bow );
        boolean b =  ranger.equipWeapon(rangerWeapon1);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void rogueEquipWeapon_correct_returnTrue() throws InvalidWeaponException {
        Rogue rogue = new Rogue("Rogue");
        Weapon rogueWeapon1 = new Weapon("rogueWeapon1",1, Slot.Weapon, 6, 8,WeaponType.Dagger);
        boolean b =  rogue.equipWeapon(rogueWeapon1);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void warriorEquipWeapon_correct_returnTrue() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Main.Warrior");
        Weapon warriorWeapon1 = new Weapon("warriorWeapon1", 1, Slot.Weapon, 10, 1,WeaponType.Axe);
        boolean b =  warrior.equipWeapon(warriorWeapon1);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void mageEquipArmor_correct_returnTrue() throws InvalidArmorException {
        Mage mage = new Mage("Mage");
        Armor mageArmor = new Armor("mageArmor",1,Slot.Head,ArmorType.Cloth,1,2,3);
        boolean b =  mage.equipArmor(mageArmor);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void rangerEquipArmor_correct_returnTrue() throws InvalidArmorException {
        Ranger ranger = new Ranger("Ranger");
        Armor rangerArmor = new Armor("rangerArmor",1, Slot.Body, ArmorType.Leather, 7,5,1);
        boolean b =  ranger.equipArmor(rangerArmor);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void rogueEquipArmor_correct_returnTrue() throws InvalidArmorException {
        Rogue rogue = new Rogue("Rogue");
        Armor rogueArmor = new Armor("rogueArmor",1, Slot.Body, ArmorType.Leather, 5,6,2);
        boolean b =  rogue.equipArmor(rogueArmor);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void warriorEquipArmor_correct_returnTrue() throws InvalidArmorException {
        Warrior warrior = new Warrior("Main.Warrior");
        Armor warriorArmor = new Armor("warriorArmor",1, Slot.Body, ArmorType.Plate, 2,7,2);
        boolean b =  warrior.equipArmor(warriorArmor);
        boolean expectedValue = true;
        assertEquals(expectedValue, b);
    }

    @Test
    public void calculateDps_noWeapon_correct(){
        Warrior warrior = new Warrior("Main.Warrior");
        float dps = (float)warrior.getDPS();
        float expectedDPS = (float)((1.0 + (5.0 / 100.0)));
        float delta = 0.0001f;
        assertEquals(expectedDPS, dps, delta);
    }

    @Test
    public void calculateDps_validWeapon_correct() throws InvalidWeaponException{
        Warrior warrior = new Warrior("Main.Warrior");
        int weaponDamage = 7;
        float attacksPerSecond = 1.1f;
        Weapon warriorWeapon1 = new Weapon("warriorWeapon1", 1, Slot.Weapon, weaponDamage,  attacksPerSecond,WeaponType.Axe);
        warrior.equipWeapon(warriorWeapon1);
        float dps = (float)warrior.getDPS();
        float warriorStrength = 5.0f;
        float expectedDPS = (float)((weaponDamage * attacksPerSecond)*(1.0 + (warriorStrength / 100.0)));
        float delta = 0.0001f;
        assertEquals(expectedDPS, dps, delta);
    }

    @Test
    public void calculateDps_validWeapon_correct2() throws InvalidWeaponException{
        Warrior warrior = new Warrior("Main.Warrior");
        Weapon warriorWeapon1 = new Weapon("Common Axe",1,Slot.Weapon,7,1.1,WeaponType.Axe);
        warrior.equipWeapon(warriorWeapon1);
        float dps = (float)warrior.getDPS();
        float expectedDPS = (float)((7.0 * 1.1)*(1.0 + (5.0 / 100.0)));
        float delta = 0.0001f;
        assertEquals(expectedDPS, dps, delta);
    }


    @Test
    public void calculateDps_validWeaponAndArmor_correct() throws InvalidWeaponException, InvalidArmorException{
        Warrior warrior = new Warrior("Main.Warrior");
        int weaponDamage = 7;
        float attacksPerSecond = 1.1f;
        Weapon warriorWeapon1 = new Weapon("warriorWeapon1", 1, Slot.Weapon, weaponDamage,  attacksPerSecond,WeaponType.Axe);
        warrior.equipWeapon(warriorWeapon1);
        int armorStrength = 1;
        float warriorStrength = 5.0f;
        Armor warriorArmor = new Armor("warriorArmor",1, Slot.Body, ArmorType.Plate, armorStrength,7,2);
        warrior.equipArmor(warriorArmor);
        float dps = (float)warrior.getDPS();
        float expectedDPS = (float)((weaponDamage * attacksPerSecond)*(1.0 + ((warriorStrength+armorStrength) / 100.0)));
        float delta = 0.0001f;
        assertEquals(expectedDPS, dps, delta);
    }

    @Test
    public void calculateDps_validWeaponAndArmor_correct2() throws InvalidWeaponException, InvalidArmorException{
        Warrior warrior = new Warrior("Main.Warrior");
        Weapon warriorWeapon1 = new Weapon("Common Axe",1,Slot.Weapon,7,1.1,WeaponType.Axe);
        warrior.equipWeapon(warriorWeapon1);
        Armor warriorArmor = new Armor("Common Plate Body Armor",1, Slot.Body,ArmorType.Plate,1,0,0);
        warrior.equipArmor(warriorArmor);
        float dps = (float)warrior.getDPS();
        float expectedDPS = (float)((7.0 * 1.1) * (1.0 + ((5.0+1.0) / 100.0)));
        float delta = 0.0001f;
        assertEquals(expectedDPS, dps, delta);
    }
}
