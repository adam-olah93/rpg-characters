package Test;

import Main.characters.*;
import org.junit.*;

public class CharacterTest {

    /** These tests check if character is level 1 when created
     Mage, Ranger, Rogue, Warrior */
    @Test
    public void mageConstructor_level_mstBeOne(){
        Mage mage = new Mage("Mage");
        int expectedLevel = 1;
        int mageLevel = mage.getLevel();
        Assert.assertEquals(expectedLevel, mageLevel);
    }

    @Test
    public void rangerConstructor_level_mustBeOne(){
        Ranger ranger = new Ranger("Ranger");
        int expectedLevel = 1;
        int rangerLevel = ranger.getLevel();
        Assert.assertEquals(expectedLevel, rangerLevel);
    }

    @Test
    public void rogueConstructor_level_mustBeOne(){
        Rogue rogue = new Rogue("Rogue");
        int expectedLevel = 1;
        int rogueLevel = rogue.getLevel();
        Assert.assertEquals(expectedLevel, rogueLevel);
    }

    @Test
    public void warriorConstructor_level_mustBeOne(){
        Warrior warrior = new Warrior("Warrior");
        int expectedLevel = 1;
        int warriorLevel = warrior.getLevel();
        Assert.assertEquals(expectedLevel, warriorLevel);
    }

    /** These tests check whether the character's level changes to 2 when gains a level
     Mage, Ranger, Rogue, Warrior */

    @Test
    public void mageLevelUp_level_mustBeTwo(){
        Mage mage = new Mage("Mage");
        mage.levelUp();
        int expectedLevel = 2;
        int mageLevel = mage.getLevel();
        Assert.assertEquals(expectedLevel, mageLevel);
    }

    @Test
    public void rangerLevelUp_level_mustBeTwo(){
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp();
        int expectedLevel = 2;
        int rangerLevel = ranger.getLevel();
        Assert.assertEquals(expectedLevel, rangerLevel);
    }

    @Test
    public void rogueLevelUp_level_mustBeTwo(){
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp();
        int expectedLevel = 2;
        int rogueLevel = rogue.getLevel();
        Assert.assertEquals(expectedLevel, rogueLevel);
    }

    @Test
    public void warriorLevelUp_level_mustBeTwo(){
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp();
        int expectedLevel = 2;
        int warriorLevel = warrior.getLevel();
        Assert.assertEquals(expectedLevel, warriorLevel);
    }

    /** These tests check whether the characters were created with the proper default attributes
     Mage, Ranger, Rogue, Warrior */

    @Test
    public void mageConstructor_baseAttributes_strengthOneDexterityOneIntelligenceEight(){
        Mage mage = new Mage("Mage");
        int expectedStrength = 1;
        int strength = mage.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 1;
        int dexterity = mage.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 8;
        int intelligence =  mage.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void rangerConstructor_baseAttributes_strengthOneDexteritySevenIntelligenceOne(){
        Ranger ranger = new Ranger("Ranger");
        int expectedStrength = 1;
        int strength = ranger.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 7;
        int dexterity = ranger.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 1;
        int intelligence =  ranger.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void rogueConstructor_baseAttributes_strengthTwoDexteritySixIntelligenceOne(){
        Rogue rogue = new Rogue("Rogue");
        int expectedStrength = 2;
        int strength = rogue.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 6;
        int dexterity = rogue.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 1;
        int intelligence =  rogue.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void warriorConstructor_baseAttributes_strengthFiveDexterityTwoIntelligenceOne(){
        Warrior warrior = new Warrior("Warrior");
        int expectedStrength = 5;
        int strength = warrior.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 2;
        int dexterity = warrior.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 1;
        int intelligence =  warrior.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    /** These tests check whether each character class has their attributes increased when leveling up
     Mage, Ranger, Rogue, Warrior */
    @Test
    public void mageLevelUp_baseAttributes_strengthTwoDexterityTwoIntelligenceThirteen(){
        Mage mage = new Mage("Mage");
        mage.levelUp();
        int expectedStrength = 2;
        int strength = mage.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 2;
        int dexterity = mage.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 13;
        int intelligence =  mage.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void rangerConstructor_baseAttributes_strengthTwoDexterityTwelveIntelligenceTwo(){
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp();
        int expectedStrength = 2;
        int strength = ranger.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 12;
        int dexterity = ranger.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 2;
        int intelligence =  ranger.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void rogueConstructor_baseAttributes_strengthThreeDexterityTenIntelligenceTwo(){
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp();
        int expectedStrength = 3;
        int strength = rogue.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 10;
        int dexterity = rogue.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 2;
        int intelligence =  rogue.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    public void warriorConstructor_baseAttributes_strengthEightDexterityFourIntelligenceTwo(){
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp();
        int expectedStrength = 8;
        int strength = warrior.getBasePrimaryAttributes().getStrength();
        int expectedDexterity = 4;
        int dexterity = warrior.getBasePrimaryAttributes().getDexterity();
        int expectedIntelligence = 2;
        int intelligence =  warrior.getBasePrimaryAttributes().getIntelligence();
        Assert.assertEquals(expectedStrength, strength);
        Assert.assertEquals(expectedDexterity, dexterity);
        Assert.assertEquals(expectedIntelligence, intelligence);
    }
}