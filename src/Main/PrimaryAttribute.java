package Main;

public class PrimaryAttribute {

  int strength;

  int dexterity;

  int intelligence;

  public PrimaryAttribute(int strength, int dexterity, int intelligence) {
    this.strength = strength;
    this.dexterity = dexterity;
    this.intelligence = intelligence;
  }

  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }

  public int getDexterity() {
    return dexterity;
  }

  public void setDexterity(int dexterity) {
    this.dexterity = dexterity;
  }

  public int getIntelligence() {
    return intelligence;
  }

  public void setIntelligence(int intelligence) {
    this.intelligence = intelligence;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append("strength").append(strength).append(", ");
    str.append("dexterity").append(dexterity).append(", ");
    str.append("intelligence").append(intelligence).append("\n");
    return str.toString();
  }
}
