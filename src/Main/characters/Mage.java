package Main.characters;

import Main.*;
import Main.exceptions.*;
import Main.items.*;

public class Mage extends Character {

  /**
   * Constructs a Mage character, setting the character's name and initializing base primary attributes.
   *
   * @param name The name of the Mage.
   */
  public Mage(String name) {
    super(name);
    this.basePrimaryAttributes = new PrimaryAttribute(1, 1, 8);
  }

  /**
   * Overrides the LevelUp method to increase the Mage's base primary attributes and level.
   * This method increments the Mage's strength, dexterity, and intelligence by specific amounts,
   * and increases the character's level by one.
   */
  @Override
  public void levelUp() {
    this.basePrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength() + 1);
    this.basePrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity() + 1);
    this.basePrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence() + 5);
    level += 1;
  }

  /**
   * Overrides the CanEquipWeapon method to enforce that Mages can only equip Wands and Staffs as weapons.
   * This method returns true if the weapon type is either a Wand or a Staff, and if the character's
   * level is greater than or equal to the weapon's required level.
   *
   * @param weapon The weapon to check for equipping.
   * @return boolean True if the weapon can be equipped by the Mage, false otherwise.
   */
  @Override
  public boolean canEquipWeapon(Weapon weapon) {
    return (weapon.getType() == WeaponType.Staff || weapon.getType() == WeaponType.Wand)
            && (this.level >= weapon.getRequiredLevelToEquip());
  }

  /**
   * Overrides the CanEquipArmor method to enforce that Mages can only equip Cloth armor.
   * This method returns true if the armor type is Cloth, and if the character's level is
   * greater than or equal to the armor's required level.
   *
   * @param armor The armor to check for equipping.
   * @return boolean True if the armor can be equipped by the Mage, false otherwise.
   */
  @Override
  public boolean canEquipArmor(Armor armor) {
    return (armor.getArmorType() == ArmorType.Cloth)
            && (this.level >= armor.getRequiredLevelToEquip());
  }

  /**
   * Overrides the EquipWeapon method to equip the given weapon if possible.
   * If the weapon is eligible for the Mage to equip (as determined by the CanEquipWeapon method),
   * the weapon is added to the Mage's equipment. If the weapon cannot be equipped, this method
   * throws an InvalidWeaponException.
   *
   * @param weapon The weapon to equip.
   * @return boolean True if the weapon was successfully equipped.
   * @throws InvalidWeaponException If the weapon cannot be equipped by the Mage.
   */
  @Override
  public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
    if (canEquipWeapon(weapon)) {
      equipments.put(Slot.Weapon, weapon);
      return true;
    } else {
      throw new InvalidWeaponException(
              "Mage can't equip the following weapon: " + weapon);
    }
  }

  /**
   * Overrides the EquipArmor method to equip the given armor if possible.
   * If the armor is eligible for the Mage to equip (as determined by the CanEquipArmor method),
   * the armor is added to the Mage's equipment. If the armor cannot be equipped, this method
   * throws an InvalidArmorException.
   *
   * @param armor The armor to equip.
   * @return boolean True if the armor was successfully equipped.
   * @throws InvalidArmorException If the armor cannot be equipped by the Mage.
   */
  @Override
  public boolean equipArmor(Armor armor) throws InvalidArmorException {
    if (canEquipArmor(armor)) {
      equipments.put(armor.getSlot(), armor);
      return true;
    } else {
      throw new InvalidArmorException("Mage can't equip the following armor: " + armor);
    }
  }

  /**
   * Overrides the GetDPS method to calculate the Mage's Damage Per Second (DPS).
   * If the Mage does not have a weapon equipped, DPS is calculated as:
   * 1 * (1 + GetTotalMainPrimaryAttribute()/100).
   * If the Mage has a weapon equipped, DPS is calculated as:
   * weapon.getDps() * (1 + GetTotalMainPrimaryAttribute()/100).
   * Each point of intelligence increases the Mage's damage by 1%.
   *
   * @return double The calculated DPS.
   */
  @Override
  public double getDPS() {
    Weapon weapon = (Weapon) equipments.get(Slot.Weapon);
    if (weapon == null) {
      return 1 * (1 + getTotalMainPrimaryAttribute() / 100);
    } else {
      return weapon.getDps() * (1 + getTotalMainPrimaryAttribute() / 100);
    }
  }

  /**
   * Overrides the GetTotalMainPrimaryAttribute method to retrieve the Mage's main attribute value.
   * For a Mage, the main primary attribute is intelligence. This method calculates the total
   * intelligence, including base and equipped bonuses.
   * Each point of intelligence increases the Mage's damage by 1%.
   *
   * @return double The total intelligence value.
   */
  @Override
  public double getTotalMainPrimaryAttribute() {
    return this.getTotalPrimaryAttributes().getIntelligence();
  }
}
