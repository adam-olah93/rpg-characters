package Main.characters;

import Main.*;
import Main.exceptions.*;
import Main.items.*;

public class Rogue extends Character {

  /**
   * Constructs a Rogue character, setting the character's name and initializing base primary attributes.
   *
   * @param name The name of the Rogue.
   */
  public Rogue(String name) {
    super(name);
    this.basePrimaryAttributes = new PrimaryAttribute(2, 6, 1);
  }

  /**
   * Overrides the LevelUp method to increase the Rogue's base primary attributes and level.
   * This method increments the Rogue's strength, dexterity, and intelligence by specific amounts,
   * and raises the character's level by one.
   */
  @Override
  public void levelUp() {
    this.basePrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength() + 1);
    this.basePrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity() + 4);
    this.basePrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence() + 1);
    level += 1;
  }

  /**
   * Overrides the CanEquipWeapon method to determine if a Rogue can equip a given weapon.
   * This method returns true if the weapon is either a Dagger or Sword and the Rogue's level is
   * greater than or equal to the required level for equipping the weapon.
   *
   * @param weapon The weapon to check.
   * @return boolean indicating whether the Rogue can equip the weapon.
   */
  @Override
  public boolean canEquipWeapon(Weapon weapon) {
    return (weapon.getType() == WeaponType.Dagger || weapon.getType() == WeaponType.Sword)
            && (this.level >= weapon.getRequiredLevelToEquip());
  }

  /**
   * Overrides the CanEquipArmor method to determine if a Rogue can equip a given armor.
   * This method returns true if the armor is either Leather or Mail and the Rogue's level is
   * greater than or equal to the required level for equipping the armor.
   *
   * @param armor The armor to check.
   * @return boolean indicating whether the Rogue can equip the armor.
   */
  @Override
  public boolean canEquipArmor(Armor armor) {
    return (armor.getArmorType() == ArmorType.Leather || armor.getArmorType() == ArmorType.Mail)
            && (this.level >= armor.getRequiredLevelToEquip());
  }

  /**
   * Overrides the EquipWeapon method to equip a weapon if possible.
   * This method equips the given weapon if it meets the requirements; otherwise, it throws an
   * InvalidWeaponException.
   *
   * @param weapon The weapon to equip.
   * @return boolean indicating whether the weapon was successfully equipped.
   * @throws InvalidWeaponException if the Rogue cannot equip the weapon.
   */
  @Override
  public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
    if (canEquipWeapon(weapon)) {
      equipments.put(Slot.Weapon, weapon);
      return true;
    } else {
      throw new InvalidWeaponException(
              "Rogue can't equip the following weapon: " + weapon);
    }
  }

  /**
   * Overrides the EquipArmor method to equip armor if possible.
   * This method equips the given armor if it meets the requirements; otherwise, it throws an
   * InvalidArmorException.
   *
   * @param armor The armor to equip.
   * @return boolean indicating whether the armor was successfully equipped.
   * @throws InvalidArmorException if the Rogue cannot equip the armor.
   */
  @Override
  public boolean equipArmor(Armor armor) throws InvalidArmorException {
    if (canEquipArmor(armor)) {
      equipments.put(armor.getSlot(), armor);
      return true;
    } else {
      throw new InvalidArmorException("Rogue can't equip the following armor: " + armor);
    }
  }

  /**
   * Overrides the GetDPS method to calculate the Rogue's damage per second (DPS).
   * If the character does not have a weapon equipped, DPS is calculated as:
   * 1 * (1 + GetTotalMainPrimaryAttribute()/100).
   * If the character has a weapon equipped, DPS is calculated as:
   * weapon.getDps() * (1 + GetTotalMainPrimaryAttribute()/100).
   * Each point of dexterity increases the Rogue's damage by 1%.
   *
   * @return The DPS value.
   */
  @Override
  public double getDPS() {
    Weapon weapon = (Weapon) equipments.get(Slot.Weapon);
    if (weapon == null) {
      return 1 * (1 + getTotalMainPrimaryAttribute() / 100);
    } else {
      return weapon.getDps() * (1 + getTotalMainPrimaryAttribute() / 100);
    }
  }

  /**
   * Overrides the GetTotalMainPrimaryAttribute method to retrieve the Rogue's primary attribute.
   * The Rogue's main attribute is dexterity, and each point of dexterity increases the Rogue's
   * damage by 1%.
   *
   * @return The total dexterity attribute value.
   */
  @Override
  public double getTotalMainPrimaryAttribute() {
    return this.getTotalPrimaryAttributes().getDexterity();
  }
}
