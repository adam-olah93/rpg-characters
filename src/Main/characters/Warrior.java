package Main.characters;

import Main.*;
import Main.exceptions.*;
import Main.items.*;

public class Warrior extends Character {

  /**
   * Constructs a Warrior character, setting the character's name and initializing base primary attributes.
   *
   * @param name The name of the Warrior.
   */
  public Warrior(String name) {
    super(name);
    this.basePrimaryAttributes = new PrimaryAttribute(5, 2, 1);
  }

  /**
   * Overrides the LevelUp method to increase the Warrior's base primary attributes and level.
   * This method increments the Warrior's strength, dexterity, and intelligence by specific amounts,
   * and raises the character's level by one.
   */
  @Override
  public void levelUp() {
    this.basePrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength() + 3);
    this.basePrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity() + 2);
    this.basePrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence() + 1);
    level += 1;
  }

  /**
   * Overrides the CanEquipWeapon method to determine if a Warrior can equip a given weapon.
   * This method returns true if the weapon is an Axe, Hammer, or Sword and the Warrior's level is
   * greater than or equal to the required level for equipping the weapon.
   *
   * @param weapon The weapon to check.
   * @return boolean indicating whether the Warrior can equip the weapon.
   */
  @Override
  public boolean canEquipWeapon(Weapon weapon) {
    return (weapon.getType() == WeaponType.Axe
            || weapon.getType() == WeaponType.Hammer
            || weapon.getType() == WeaponType.Sword)
            && (this.level >= weapon.getRequiredLevelToEquip());
  }

  /**
   * Overrides the CanEquipArmor method to determine if a Warrior can equip a given armor.
   * This method returns true if the armor is either Mail or Plate and the Warrior's level is
   * greater than or equal to the required level for equipping the armor.
   *
   * @param armor The armor to check.
   * @return boolean indicating whether the Warrior can equip the armor.
   */
  @Override
  public boolean canEquipArmor(Armor armor) {
    return (armor.getArmorType() == ArmorType.Mail || armor.getArmorType() == ArmorType.Plate)
            && (this.level >= armor.getRequiredLevelToEquip());
  }

  /**
   * Overrides the EquipWeapon method to equip a weapon if possible.
   * This method equips the given weapon if it meets the requirements; otherwise, it throws an
   * InvalidWeaponException.
   *
   * @param weapon The weapon to equip.
   * @return boolean indicating whether the weapon was successfully equipped.
   * @throws InvalidWeaponException if the Warrior cannot equip the weapon.
   */
  @Override
  public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
    if (canEquipWeapon(weapon)) {
      equipments.put(Slot.Weapon, weapon);
      return true;
    } else {
      throw new InvalidWeaponException(
              "Warrior can't equip the following weapon: " + weapon);
    }
  }

  /**
   * Overrides the EquipArmor method to equip armor if possible.
   * This method equips the given armor if it meets the requirements; otherwise, it throws an
   * InvalidArmorException.
   *
   * @param armor The armor to equip.
   * @return boolean indicating whether the armor was successfully equipped.
   * @throws InvalidArmorException if the Warrior cannot equip the armor.
   */
  @Override
  public boolean equipArmor(Armor armor) throws InvalidArmorException {
    if (canEquipArmor(armor)) {
      equipments.put(armor.getSlot(), armor);
      return true;
    } else {
      throw new InvalidArmorException(
              "Warrior can't equip the following armor: " + armor);
    }
  }

  /**
   * Overrides the GetDPS method to calculate the Warrior's damage per second (DPS).
   * If the character does not have a weapon equipped, DPS is calculated as:
   * 1 * (1 + GetTotalMainPrimaryAttribute()/100).
   * If the character has a weapon equipped, DPS is calculated as:
   * weapon.getDps() * (1 + GetTotalMainPrimaryAttribute()/100).
   * Each point of strength increases the Warrior's damage by 1%.
   *
   * @return The DPS value.
   */
  @Override
  public double getDPS() {
    Weapon weapon = (Weapon) equipments.get(Slot.Weapon);
    if (weapon == null) {
      return 1 * (1 + getTotalMainPrimaryAttribute() / 100);
    } else {
      return weapon.getDps() * (1 + getTotalMainPrimaryAttribute() / 100);
    }
  }

  /**
   * Overrides the GetTotalMainPrimaryAttribute method to retrieve the Warrior's primary attribute.
   * The Warrior's main attribute is strength, and each point of strength increases the Warrior's
   * damage by 1%.
   *
   * @return The total strength attribute value.
   */
  @Override
  public double getTotalMainPrimaryAttribute() {
    return this.getTotalPrimaryAttributes().getStrength();
  }
}

