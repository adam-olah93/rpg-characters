package Main.characters;

import Main.*;
import Main.exceptions.*;
import Main.items.*;

public class Ranger extends Character {

    /**
     * Constructs a Ranger character, setting the character's name and initializing base primary attributes.
     *
     * @param name The name of the Ranger.
     */
    public Ranger(String name) {
        super(name);
        this.basePrimaryAttributes = new PrimaryAttribute(1, 7, 1);
    }

    /**
     * Overrides the LevelUp method to increase the Ranger's base primary attributes and level.
     * This method increments the Ranger's strength, dexterity, and intelligence by fixed amounts,
     * and increases the character's level by one.
     */
    @Override
    public void levelUp() {
        this.basePrimaryAttributes.setStrength(this.basePrimaryAttributes.getStrength() + 1);
        this.basePrimaryAttributes.setDexterity(this.basePrimaryAttributes.getDexterity() + 5);
        this.basePrimaryAttributes.setIntelligence(this.basePrimaryAttributes.getIntelligence() + 1);
        level += 1;
    }

    /**
     * Overrides the CanEquipWeapon method to ensure that Rangers can only equip Bows.
     * This method returns true if the weapon type is a Bow, and if the Ranger's level is
     * greater than or equal to the weapon's required level.
     *
     * @param weapon The weapon to check for equipping.
     * @return boolean True if the weapon can be equipped by the Ranger, false otherwise.
     */
    @Override
    public boolean canEquipWeapon(Weapon weapon) {
        return (weapon.getType() == WeaponType.Bow) && (this.level >= weapon.getRequiredLevelToEquip());
    }

    /**
     * Overrides the CanEquipArmor method to ensure that Rangers can only equip Leather or Mail armor.
     * This method returns true if the armor type is Leather or Mail, and if the Ranger's level is
     * greater than or equal to the armor's required level.
     *
     * @param armor The armor to check for equipping.
     * @return boolean True if the armor can be equipped by the Ranger, false otherwise.
     */
    @Override
    public boolean canEquipArmor(Armor armor) {
        return (armor.getArmorType() == ArmorType.Leather || armor.getArmorType() == ArmorType.Mail)
                && (this.level >= armor.getRequiredLevelToEquip());
    }

    /**
     * Overrides the EquipWeapon method to equip the specified weapon if allowed.
     * If the weapon meets the criteria for equipping (as determined by the CanEquipWeapon method),
     * the weapon is equipped. Otherwise, this method throws an InvalidWeaponException.
     *
     * @param weapon The weapon to equip.
     * @return boolean True if the weapon was successfully equipped.
     * @throws InvalidWeaponException If the weapon cannot be equipped by the Ranger.
     */
    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (canEquipWeapon(weapon)) {
            equipments.put(Slot.Weapon, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("Ranger can't equip the following weapon: " + weapon);
        }
    }

    /**
     * Overrides the EquipArmor method to equip the specified armor if allowed.
     * If the armor meets the criteria for equipping (as determined by the CanEquipArmor method),
     * the armor is equipped. Otherwise, this method throws an InvalidArmorException.
     *
     * @param armor The armor to equip.
     * @return boolean True if the armor was successfully equipped.
     * @throws InvalidArmorException If the armor cannot be equipped by the Ranger.
     */
    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if (canEquipArmor(armor)) {
            equipments.put(armor.getSlot(), armor);
            return true;
        } else {
            throw new InvalidArmorException("Ranger can't equip the following armor: " + armor.getArmorType());
        }
    }

    /**
     * Overrides the GetDPS method to calculate the Ranger's Damage Per Second (DPS).
     * If the Ranger does not have a weapon equipped, DPS is calculated using:
     * 1 * (1 + GetTotalMainPrimaryAttribute()/100).
     * If the Ranger has a weapon equipped, DPS is calculated using:
     * weapon.getDps() * (1 + GetTotalMainPrimaryAttribute()/100).
     * Each point of dexterity increases the Ranger's damage by 1%.
     *
     * @return double The calculated DPS.
     */
    @Override
    public double getDPS() {
        Weapon weapon = (Weapon) equipments.get(Slot.Weapon);
        if (weapon == null) {
            return 1 * (1 + getTotalMainPrimaryAttribute() / 100);
        } else {
            return weapon.getDps() * (1 + getTotalMainPrimaryAttribute() / 100);
        }
    }

    /**
     * Overrides the GetTotalMainPrimaryAttribute method to retrieve the Ranger's main attribute value.
     * For a Ranger, the main primary attribute is dexterity. This method calculates the total
     * dexterity, including base and equipped bonuses.
     * Each point of dexterity increases the Ranger's damage by 1%.
     *
     * @return double The total dexterity value.
     */
    @Override
    public double getTotalMainPrimaryAttribute() {
        return this.getTotalPrimaryAttributes().getDexterity();
    }
}
