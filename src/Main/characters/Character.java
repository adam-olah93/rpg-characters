package Main.characters;

import Main.PrimaryAttribute;
import Main.Slot;
import Main.exceptions.*;
import Main.items.*;
import java.util.HashMap;

public abstract class Character {

  String name;

  int level;

  HashMap<Slot, Item> equipments;

  PrimaryAttribute basePrimaryAttributes;

  PrimaryAttribute totalPrimaryAttributes;

  public Character(String name) {
    this.name = name;
    this.level = 1;
    equipments = new HashMap<Slot, Item>();
  }

  public PrimaryAttribute getBasePrimaryAttributes() {
    return basePrimaryAttributes;
  }

  /**
   * Calculates the total primary attributes, including base attributes and those from equipped
   * armor. This method creates a temporary instance of `PrimaryAttribute` initialized with the base
   * primary attributes. It then iterates over all equipped items (accessed via
   * `equipments.values()`). For each item, if it is of type `Armor`, the method adds the armor's
   * attributes to the temporary `PrimaryAttribute` instance. The method returns the aggregated
   * primary attributes.
   *
   * @return A `PrimaryAttribute` object representing the total primary attributes.
   */
  public PrimaryAttribute getTotalPrimaryAttributes() {
    PrimaryAttribute temp =
        new PrimaryAttribute(
            basePrimaryAttributes.getStrength(),
            basePrimaryAttributes.getDexterity(),
            basePrimaryAttributes.getIntelligence());
    for (Item i : equipments.values()) {
      if (i instanceof Armor a) {
        temp.setStrength(temp.getStrength() + a.getAttributes().getStrength());
        temp.setDexterity(temp.getDexterity() + a.getAttributes().getDexterity());
        temp.setIntelligence(temp.getIntelligence() + a.getAttributes().getIntelligence());
      }
    }
    return temp;
  }

  public String getName() {
    return name;
  }

  public int getLevel() {
    return level;
  }

  public abstract void levelUp();

  public abstract boolean canEquipWeapon(Weapon weapon);

  public abstract boolean canEquipArmor(Armor armor);

  public abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;

  public abstract boolean equipArmor(Armor armor) throws InvalidArmorException;

  public abstract double getDPS();

  public abstract double getTotalMainPrimaryAttribute();

  @Override
  public String toString() {
    PrimaryAttribute temp = this.getTotalPrimaryAttributes();
    return String.format(
        "Character name: %s, level: %d, strength: %d, dexterity: %d, intelligence: %d, dps: %.2f",
        name, level, temp.getStrength(), temp.getDexterity(), temp.getIntelligence(), getDPS());
  }
}
