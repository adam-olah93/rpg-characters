package Main;

import Main.characters.*;
import Main.characters.Character;
import Main.exceptions.*;
import Main.items.*;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    try {
      Mage mage = new Mage("Mage");
      Ranger ranger = new Ranger("Ranger");
      Rogue rogue = new Rogue("Rogue");
      Warrior warrior = new Warrior("Warrior");

      Weapon[] weapons = {
              new Weapon("Inferno Staff", 1, Slot.Weapon, 5, 2, WeaponType.Staff),
              new Weapon("Twilight Wand", 1, Slot.Weapon, 4, 2, WeaponType.Wand),
              new Weapon("Shadowstrike Bow", 1, Slot.Weapon, 6, 4, WeaponType.Bow),
              new Weapon("Venomfang Dagger", 2, Slot.Weapon, 6, 8, WeaponType.Dagger),
              new Weapon("Ebonblade Sword", 1, Slot.Weapon, 8, 3, WeaponType.Sword),
              new Weapon("Stormcleaver Axe", 3, Slot.Weapon, 10, 1, WeaponType.Axe),
              new Weapon("Thunderstrike Hammer", 1, Slot.Weapon, 7, 1, WeaponType.Hammer),
              new Weapon("Bloodreaver Sword", 2, Slot.Weapon, 4, 2, WeaponType.Sword)
      };

      printMenu();

      Scanner sc = new Scanner(System.in);
      int number = sc.nextInt();

      while (number != 0) {
        handleMenuOption(number, mage, ranger, rogue, warrior, weapons, sc);
        printMenu();
        number = sc.nextInt();
      }
    } catch (Exception ex) {
      System.out.println("Exception was thrown, the exception: " + ex.getMessage());
    }
  }

  private static void handleMenuOption(
      int number,
      Mage mage,
      Ranger ranger,
      Rogue rogue,
      Warrior warrior,
      Weapon[] weapons,
      Scanner sc) {
    switch (number) {
      case 1 -> System.out.println(mage);
      case 2 -> System.out.println(ranger);
      case 3 -> System.out.println(rogue);
      case 4 -> System.out.println(warrior);
      case 5 -> {
        mage.levelUp();
        System.out.println("You have successfully leveled up to " + mage.getLevel() + "!");
      }
      case 6 -> {
        ranger.levelUp();
        System.out.println("You have successfully level up to " + ranger.getLevel() + "!");
      }
      case 7 -> {
        rogue.levelUp();
        System.out.println("You have successfully leveled up to " + rogue.getLevel() + "!");
      }
      case 8 -> {
        warrior.levelUp();
        System.out.println("You have successfully leveled up to " + warrior.getLevel() + "!");
      }
      case 9 -> equipWeapon(mage, weapons[0]);
      case 10 -> equipWeapon(mage, weapons[1]);
      case 11 -> equipWeapon(ranger, weapons[2]);
      case 12 -> equipWeapon(rogue, weapons[3]);
      case 13 -> equipWeapon(rogue, weapons[4]);
      case 14 -> equipWeapon(rogue, weapons[1]);
      case 15 -> equipWeapon(warrior, weapons[5]);
      case 16 -> equipWeapon(warrior, weapons[6]);
      case 17 -> equipWeapon(warrior, weapons[7]);
      case 18 -> equipArmor(mage, sc);
      case 19 -> equipArmor(ranger, sc);
      case 20 -> equipArmor(rogue, sc);
      case 21 -> equipArmor(warrior, sc);
      default -> System.out.println("Wrong number entered");
    }
  }

  private static void equipWeapon(Character character, Weapon weapon) {
    try {
      character.equipWeapon(weapon);
      System.out.println("You have successfully equipped the weapon: " + weapon.getName());
    } catch (InvalidWeaponException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void equipArmor(Character character, Scanner sc) {
    try {
      System.out.println("Where to equip armor?");
      System.out.println("1. Head");
      System.out.println("2. Body");
      System.out.println("3. Legs");
      Slot slot = getSlot(sc.nextInt());

      System.out.println("Which type of armor?");
      ArmorType armorType = getArmorType(character, sc);

      if (slot != null && armorType != null) {
        Armor armor = new Armor(character.getName() + "Armor", 1, slot, armorType, 2, 7, 2);
        character.equipArmor(armor);
        System.out.println(
            "You have successfully equipped the armor: " + armorType + " to " + slot + "!");
      } else {
        System.out.println("Failed to equip armor.");
      }
    } catch (InvalidArmorException e) {
      System.out.println(e.getMessage());
    }
  }

  private static Slot getSlot(int slotNumber) {
    return switch (slotNumber) {
      case 1 -> Slot.Head;
      case 2 -> Slot.Body;
      case 3 -> Slot.Legs;
      default -> null;
    };
  }

  private static ArmorType getArmorType(Character character, Scanner sc) {
    if (character instanceof Mage) {
      return ArmorType.Cloth;
    } else {
      System.out.println("1. Leather");
      System.out.println("2. Mail");
      if (character instanceof Warrior) {
        System.out.println("3. Plate");
      }
      int armorNumber = sc.nextInt();
      return switch (armorNumber) {
        case 1 -> ArmorType.Leather;
        case 2 -> ArmorType.Mail;
        case 3 -> (character instanceof Warrior) ? ArmorType.Plate : null;
        default -> null;
      };
    }
  }

  private static void printMenu() {
    System.out.println("Menu");
    System.out.println("0.  Exit");
    System.out.println("1.  Display mage stats");
    System.out.println("2.  Display ranger stats");
    System.out.println("3.  Display rogue stats");
    System.out.println("4.  Display warrior stats");
    System.out.println("5.  Level up mage");
    System.out.println("6.  Level up ranger");
    System.out.println("7.  Level up rogue");
    System.out.println("8.  Level up warrior");
    System.out.println("9.  Equip staff for mage");
    System.out.println("10. Equip wand for mage");
    System.out.println("11. Equip bow for ranger");
    System.out.println("12. Equip dagger for rogue");
    System.out.println("13. Equip sword for rogue");
    System.out.println("14. Equip wand for rogue");
    System.out.println("15. Equip axe for warrior");
    System.out.println("16. Equip hammer for warrior");
    System.out.println("17. Equip sword for warrior");
    System.out.println("18. Equip armor for mage");
    System.out.println("19. Equip armor for ranger");
    System.out.println("20. Equip armor for rogue");
    System.out.println("21. Equip armor for warrior");
    System.out.println("Enter a number: ");
  }
}