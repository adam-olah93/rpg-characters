package Main.items;

import Main.PrimaryAttribute;
import Main.Slot;

public class Armor extends Item {

  ArmorType type;

  PrimaryAttribute attributes;

  public Armor(
      String name,
      int requiredLevelToEquip,
      Slot slot,
      ArmorType type,
      int strength,
      int dexterity,
      int intelligence) {
    super(name, requiredLevelToEquip, slot);
    this.type = type;
    this.attributes = new PrimaryAttribute(strength, dexterity, intelligence);
  }

  public String getName() {
    return super.getName();
  }

  public void setName(String name) {
    super.setName(name);
  }

  public Slot getSlot() {
    return super.getSlot();
  }

  public void setSlot(Slot slot) {
    super.setSlot(slot);
  }

  public ArmorType getArmorType() {
    return type;
  }

  public void setArmorType(ArmorType type) {
    this.type = type;
  }

  public PrimaryAttribute getAttributes() {
    return attributes;
  }

  public void setAttributes(PrimaryAttribute attributes) {
    this.attributes = attributes;
  }

  @Override
  public String toString() {
    return String.format(
        "Armor: %s, required level to equip: %d, strength: %d, dexterity: %d, intelligence: %d, slot: %s, type: %s",
        this.name,
        this.requiredLevelToEquip,
        this.attributes.getStrength(),
        this.attributes.getDexterity(),
        this.attributes.getIntelligence(),
        this.slot,
        this.type);
  }
}
