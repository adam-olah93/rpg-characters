package Main.items;

import Main.Slot;

public class Weapon extends Item {

  int damage;

  double attacksPerSecond;

  double DPS;

  WeaponType type;

  public Weapon(
      String name,
      int requiredLevelToEquip,
      Slot slot,
      int damage,
      double attacksPerSecond,
      WeaponType type) {
    super(name, requiredLevelToEquip, slot);
    this.attacksPerSecond = attacksPerSecond;
    this.damage = damage;
    this.DPS = this.damage * this.attacksPerSecond;
    this.type = type;
  }

  public String getName() {
    return super.getName();
  }

  public void setName(String name) {
    super.setName(name);
  }

  public Slot getSlot() {
    return super.getSlot();
  }

  public void setSlot(Slot slot) {
    super.setSlot(slot);
  }

  public WeaponType getType() {
    return type;
  }

  public void setType(WeaponType type) {
    this.type = type;
  }

  public double getDps() {
    this.DPS = this.damage * this.attacksPerSecond;
    return DPS;
  }

  @Override
  public String toString() {
    return String.format(
        "Weapon name: %s, required level to equip: %d, attacks per second: %.2f, damage: %d, dps: %.2f, slot: %s, type: %s",
        this.name,
        this.requiredLevelToEquip,
        this.attacksPerSecond,
        this.damage,
        this.DPS,
        this.slot,
        this.type);
  }
}
