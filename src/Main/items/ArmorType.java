package Main.items;

public enum ArmorType {
  Cloth,
  Leather,
  Mail,
  Plate
}
