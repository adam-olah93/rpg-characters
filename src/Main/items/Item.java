package Main.items;

import Main.Slot;

public abstract class Item {

  String name;

  int requiredLevelToEquip;

  Slot slot;

  public Item(String name, int requiredLevelToEquip, Slot slot) {
    this.name = name;
    this.requiredLevelToEquip = requiredLevelToEquip;
    this.slot = slot;
  }

  public Item() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRequiredLevelToEquip() {
    return requiredLevelToEquip;
  }

  public void setRequiredLevelToEquip(int requiredLevelToEquip) {
    this.requiredLevelToEquip = requiredLevelToEquip;
  }

  public Slot getSlot() {
    return slot;
  }

  public void setSlot(Slot slot) {
    this.slot = slot;
  }
}
