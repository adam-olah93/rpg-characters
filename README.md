# RPG-Characters Game

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Java Console Application** with **Unit Testing** for the Java Assignment 1
(Noroff Accelerate Full-Stack course)

## Table of Contents

* [Description](#description)
* [Technologies](#technologies)
* [Folder Structure](#folder-structure)
* [Class Diagram](#class-diagram)
* [Installation](#installation)
* [Usage](#usage)
* [Maintainer](#maintainer)

## Description

In my game (Console Application), there are four classes that a character can be: Mage, Ranger, Rogue, and Warrior.
Characters have several types of attributes that represent different aspects of them. These attributes determine their
power. As characters level up, their attributes also increase. Characters can equip items, which can be weapons or
armor. Equipped items also increase their attributes.

The program prints out a menu from which we can choose our character's next step. We can display our character's
statistics, level up our character, or equip different types of weapons and armor.

![Menu.PNG](doc/Menu.PNG)

**Character classes can be chosen:**

* Mage
* Ranger
* Rogue
* Warrior

**Weapons can be equipped:**

* Awe
* Bow
* Dagger
* Hammer
* Staff
* Sword
* Wand

**Armors can be equipped:**

* Cloth
* Leather
* Mail
* Plate

## Technologies

* IntelliJ IDEA (JDK - 21, Oracle OpenJDK version)
* JUnit 5
* Java 21 LTS

## Folder Structure

The folder structure of the app at high level is as follows:

.                         
│      
├── doc             
├── src                                                                 
└── README.md

## Class Diagram

![ClassDiagram.PNG](doc/ClassDiagram.PNG)

## Installation

* Install IntelliJ IDEA (at least JDK 17)
* JUnit 5

## Usage

* Clone the repository - https://gitlab.com/adam-olah93/rpg-characters
* Run the application
* Run tests by right-clicking on the tests folder and selecting Run 'All Tests'

## Maintainer

@adam-olah93